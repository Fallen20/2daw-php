<?php include("./Class/ProductList.php");?>

<!doctype html>
<html>
<?php
  session_start();
  $product=new ProductList();//iniciar clase
  $product->loadShoppingList();

  

?>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Product List</title>
    <!-- Styles & JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">
</head>

<body>

    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <div class="container">
            <a class="navbar-brand" href="index.php">Tienda</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarResponsive">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="#">Numero de produtos: <span id="carrito"><?php echo $_SESSION['elementosComprados']?></span> | </a>
                       
                    </li>
                    <li class="nav-item active"> <a class="nav-link" href="clearCart.php">Vaciar carrito  |
                        </a>
                    </li>
                    <li class="nav-item active">
                        <a class="nav-link" href="resumen.php">Finalizar compra
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
<div class="row">

  <div class="col-lg-3">

    <h1 class="my-4">Resumen</h1>
    <ul class="list-group">
      <li class="list-group-item">
        Total de productos: <?php echo $_SESSION['elementosComprados'];?></li>
      <li class="list-group-item">
        Precio total de productos: <?php echo $_SESSION['precio'];?> €</li>
    </ul>



  </div>
  <!-- /.col-lg-3 -->

  <div class="col-lg-9">
    <div class="row">

    <?php
      if(sizeof($product->getShoppingList())==0){
    ?>
       <!-- SI no hay productos -->
       <p>No products to show</p>

     <?php
    }
    else{
      foreach($product->getShoppingList() as $prod){
    ?>

    <!-- Por cada producto en el carrito -->
      <div class="col-lg-4 col-md-6 mb-4">
        <div class="card h-100">
          <a href="#"><img class="card-img-top" src="<?php echo $prod->getFoto();?>" alt=""></a>
          <div class="card-body">
            <h3 class="card-title">
              <a href="#"><?php echo $prod->getNombre();?></a>
            </h3>
            
            <?php
                echo '<h4>Comprar: '.$prod->getCantidadCompra().' productos</h4>';
                echo '<h5>'.$prod->getPrecioCompra()*$prod->getCantidadCompra().' €</h5>';
                
                echo '<h4>Alquiler: '.$prod->getCantidadAlquiler().' productos </h4>';
                echo '<h5>'.$prod->getPrecioAlquiler()*$prod->getCantidadAlquiler().' €</h5>';
              
            ?>
            
          </div>
        </div>
      </div>
      <?php
        }
      }
    ?>

    </div>
    <!-- /.row -->

  </div>
  <!-- /.col-lg-9 -->
</div>
