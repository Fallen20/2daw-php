<?php

    class Product{

        private $nombre;
        private $foto;
        private $descripcion;
        private $precioCompra;
        private $precioAlquiler;
        private $tipoCompra;
        private $cantidadCompra;
        private $cantidadAlquiler;

        /*Constructor*/
        function __construct($nombre, $foto,$descripcion, $precioCompra, $precioAlquiler){
            $this->nombre=$nombre;
            $this->foto=$foto;
            $this->descripcion=$descripcion;
            $this->precioCompra=$precioCompra;
            $this->precioAlquiler=$precioAlquiler;

        }    
        public function getNombre(){return $this->nombre;}
        public function getFoto(){return $this->foto;}
        public function getDesc(){return $this->descripcion;}
        public function getPrecioCompra(){return $this->precioCompra;}
        public function getPrecioAlquiler(){return $this->precioAlquiler;}
        public function getTipo(){return $this->tipoCompra;}
        public function getCantidadCompra(){return $this->cantidadCompra;}
        public function getCantidadAlquiler(){return $this->cantidadAlquiler;}
        
        public function setTipo($tipo){$this->tipoCompra=$tipo;}
        public function setCantidadCompra($cantidad){$this->cantidadCompra=$cantidad;}
        public function setCantidadAlquiler($cantidad){$this->cantidadAlquiler=$cantidad;}

    }
?>