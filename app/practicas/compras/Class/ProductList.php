<?php
include("ProductsClass.php");

    class ProductList{
        private $productosList=[];
        private $archivoName;
        private $shoppingList=[];

        function __construct(){
          // $this->archivoName=$_SERVER["DOCUMENT_ROOT"]."/practicas/compras/assets/products.db";
          $this->archivoName=$_SERVER["DOCUMENT_ROOT"]."/myProyect/practicas/compras/assets/products.db";
          $this->loadProductList();
      }

        function loadProductList(){
          //mirar si existe
          if(file_exists($this->archivoName)){
          try{
                //abrir el archivo
                $archivo = fopen($this->archivoName, "r");

                //leer
                $lecturaTotal=fread($archivo, filesize($this->archivoName));
  
                //separar
                $separadoEnter=explode("\n",$lecturaTotal);
  
                for ($contador=0; $contador<count($separadoEnter); $contador++){
                  //mirar si no es un espacio en blanco o nulo o enter
                    //separar por ###
                    $separado=explode("###",$separadoEnter[$contador]);
      
                    //crear un elemento producto
                    $producto=new Product($separado[0],$separado[1],$separado[2],$separado[3],$separado[4]);
                  //nombre,imagen, descripcion, precio compra, precio descuento

                    //añadirlo a la array
                    array_push($this->productosList,$producto);
                    
                  }
      
                  //lo cierra
                  fclose($archivo);
                  
                } catch (Exception $e) {$this->error = $e->getMessage();}
            }
    
          }
          
          
          public function loadShoppingList(){
            //hacer un for de los productos y luego otro con el session

            //vaciar el array anterior porque sino se duplican
            $this->shoppingList=[];
            
            //mirar si existe
            if(file_exists($this->archivoName)){
              try{
                //abrir el archivo
                $archivo = fopen($this->archivoName, "r");
                
                //leer
                $lecturaTotal=fread($archivo, filesize($this->archivoName));
                
                //separar archivo texto
                $separadoEnter=explode("\n",$lecturaTotal);
                
                //for del text
                $contador2=0;
                
                  foreach($_SESSION['nombres'] as $key => $value) {
                    
                    for ($contador=0; $contador<count($separadoEnter); $contador++){
                      //separar por ###
                      $separado=explode("###",$separadoEnter[$contador]);

                      //for del sesion
                      if($value==$separado[0]){//si es la misma imagen
                        
                        //crear el objeto producto
                        $prod=new Product($separado[0],$separado[1],$separado[2],$separado[3],$separado[4]);

                        //nombre,imagen, descripcion, precio compra, precio descuento

                        if(isset($_SESSION['cantidadCompra'][$contador2])){$prod->setCantidadCompra($_SESSION['cantidadCompra'][$contador2]);}
                        else{$prod->setCantidadCompra(0);}

                        if(isset($_SESSION['cantidadAlquiler'][$contador2])){$prod->setCantidadAlquiler($_SESSION['cantidadAlquiler'][$contador2]);}
                        else{$prod->setCantidadAlquiler(0);}
                        //rento o buy

                        // echo $contador2;die();
                                                
                        //añadir a la array
                        array_push($this->shoppingList,$prod);
                        
                        $contador2++;
                      }
                      
                    } 

                  }


                

                //lo cierra
                fclose($archivo);

              } catch (Exception $e) {$this->error = $e->getMessage();}
            }

          }
          
          public function getList(){return $this->productosList;}
          public function getShoppingList(){return $this->shoppingList;}
           

    }
?>