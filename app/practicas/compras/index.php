<?php include("./Class/ProductList.php");?>
<?php include("./Class/functionsClass.php");?>
<?php 
    session_start();

 //si no existe, crealo
 if(!isset($_SESSION['cantidadAlquiler'])){
  $_SESSION['elementosComprados']=0;//crear var
  $_SESSION['precio']=0;//crear var
  $_SESSION['nombres']=[];//crear var
  $_SESSION['tipo']=[];//crear var
  $_SESSION['cantidadCompra']=[];//crear var 
  $_SESSION['cantidadAlquiler']=[];//crear var 
}

    $product=new ProductList();//iniciar clase
?>
    
<!doctype html>
<html>
  

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Product List</title>
    <!-- Styles & JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">
</head>

<body>

    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <div class="container">
            <a class="navbar-brand" href="index.php">Tienda</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarResponsive">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="#">Numero de produtos: 
                          <span id="carrito"><?php echo $_SESSION['elementosComprados']?></span>
                           | </a>
                    </li>
                    <li class="nav-item active"> <a class="nav-link" href="clearCart.php">Vaciar carrito  |
                        </a><!-- preguntar -->
                    </li>
                    <li class="nav-item active">
                        <a class="nav-link" href="resumen.php">Finalizar compra
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <div class="container">
            <a class="navbar-brand" href="index.php">Product's List</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
        </div>
    </nav>

    <!-- Page Content -->
    <div class="container m-3">
    <div class="row">
      
    <?php
      if(sizeof($product->getList())==0){
    ?>
       <!-- SI no hay productos -->
       <p>No products to show</p>

     <?php
    }
    else{
      foreach($product->getList() as $prod){
    ?>

     <!--
     *  Por cada producto
      *-->
      <div class="col-lg-4 col-md-6 mb-4">
        <div class="card h-100">
          <img class="card-img-top" src="<?php echo $prod->getFoto();?>" alt=""></img>
          <div class="card-body">
            <h4 class="card-title">
              <a href="#"><?php echo $prod->getNombre();?></a>
            </h4>
            <h5 id="precio">Compra: <?php echo $prod->getPrecioCompra();?>€</h5>
            <h5 id="precio">Alquiler: <?php echo $prod->getPrecioAlquiler();?>€</h5>
            <p class="card-text"><?php echo $prod->getDesc();?></p>
          </div>
          <div class="card-footer">
            <form class="addCart" action="productManager.php" method="post">
              <input type="hidden" name="id" value="<?php echo $prod->getNombre();?>">
              <input type="hidden" name="quantity" value="1">
              <input type="hidden" name="action" value="buy">
              <input type="hidden" name="precio" value="<?php echo $prod->getPrecioCompra();?>"></input>
              <button type="submit" class="btn-block btn-primary">Comprar</button>
            </form>
            <br>
            <form class="addCart" action="productManager.php" method="post">
              <input type="hidden" name="id" value="<?php echo $prod->getNombre();?>">
              <input type="hidden" name="quantity" value="1">
              <input type="hidden" name="action" value="rent">
              <input type="hidden" name="precio" value="<?php echo $prod->getPrecioAlquiler();?>"></input>
              <button type="submit" class="btn-block btn-primary">Alquilar</button>
            </form>
          </div>
        </div>
      </div>
      <?php
        }
      }
    ?>

    </div>
    </div>
    <!-- /.container -->

    <!-- Footer -->
    <footer class="py-5 bg-dark">
        <div class="container">
            <p class="m-0 text-center text-white">Copyright &copy; Escola del Treball 2022</p>
        </div>
        <!-- /.container -->
    </footer>
</body>

</html>