<?php
include('./Class/UploadClass.php');//incluye el php
//variables constantes
define("PICTURE_NAME", "picture");
define("PICTURE_TITLE", "title");
define('TITLE_ERROR', "Please write a title for the picture");


// Check if the form was submitted
if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_FILES[PICTURE_NAME])) {
    //check if title is in the form. NO TOCAR
    if (empty($_POST[PICTURE_TITLE])) {
        header('Location: index.php?upload=error&msg=' . urlencode(TITLE_ERROR));
        return;
    }
   
    //HACER:
    //If File is uploaded correcty, we added to our file and redirect
    //$upload = new Upload(.....);


    //con esto ya se sube y se escribe porque se llama al metodo en el constructor
    $upload=new Upload($_POST[PICTURE_TITLE],$_FILES[PICTURE_NAME]);



     //check if any error in the upload process. IF OK redirect to Success if not redirect to error. 
     //NO TOCAR
    if ($upload->getError() != null){
        // header('Location: index.php?upload=error&msg=' . urlencode($upload->getError()));
        header('Location: index.php?upload=error&msg=' . urlencode($upload->getError()));
    }
    else{
        header("Location: index.php?upload=success");
    }
}
