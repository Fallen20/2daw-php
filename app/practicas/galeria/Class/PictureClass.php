
<?php
    class Picture{
        private $title;
        private $fileName;

        /*Constructor*/
        function __construct($title, $fileName){
            $this->title=$title;
            $this->fileName=$fileName;
        }
    
        /*
      *Getters. Lo que quiere decir que los atributos de
      *title y filename son private
      */
        public function getTitle(){return $this->title;}
        public function getFileName(){return $this->fileName;}
    }
?>