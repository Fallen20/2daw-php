<?php
define("MAX_SIZE", 5); //Max Mb
define("UPLOAD_FOLDER", "pictures/");
define("PICTURES_LIST", "pictures/list.txt");

    class UploadError extends Exception{}
    class Upload{
        private $fileName = null;//titulo que le ponemos
        private $file = null;//archivo en general
        private $error = null;
        private $rutaArchivo = null;

        /*
        * Constructor. NO tocar
        */
        function __construct($fileName, $file){
            $this->fileName = $fileName;
            $this->file = $file;

            $this->rutaArchivo=$this->uploadPicture();

            if($this->rutaArchivo!=null){
                $this->addPictureToFile($this->rutaArchivo,$this->fileName);
            }
        }
        
        /*
        * Función que se encarga de subir un archivo y moverlo a la carpeta /pictures
        * que almacena todas las fotos.
        * Return: Devuelve la ruta final del archivo.
        */
        function uploadPicture(){

            //rutas 
             $rutaCarpeta = $_SERVER['DOCUMENT_ROOT']."/practicas/galeria/pictures";
             $rutaFinal = $rutaCarpeta."/".$this->file['name'];
            
            
             $rutaReturn = "/practicas/galeria/pictures/".$this->file['name'];
            
            //extension de la foto
            $extensionImagen = strtolower(pathinfo($this->file['name'],PATHINFO_EXTENSION));
            
            try{
                
                //las comprobaciones han de ser la contrario, asi si peta en una ya no sigue y no se hace escalera
                
                //mirar si el folder existe
                if(!is_dir($rutaCarpeta)){throw new UploadError("ERROR - La carpeta no existe");}
                
                //mirar si tmb name existe
                if($this->file['tmp_name']==null){throw new UploadError("ERROR - No se ha subido correctamente el archivo");}
                
                // //miro el archivo es una foto
                if($extensionImagen!="jpg" && $extensionImagen!="png" && $extensionImagen!="jpeg" && $extensionImagen!="gif"){throw new UploadError("ERROR - El archivo ha de tener una extensión correcta (jpg, jpeg,png, gif)");}
                
                // //mirar que el tamaño no es superior a 5mb
                if($this->file['size']>5*1048576){throw new UploadError("ERROR - No puede ser mayor a 5mb.");}

                // mirar el myme
                $mime=explode("/",$this->file['type']);
                
                if(in_array('image',$mime)!=-1) {throw new UploadError("ERROR - Myme equivocado");}//-1 es que no esta
                
                //mirar si existe
                
                 if(file_exists($rutaFinal)==true) {throw new UploadError("ERROR - El archivo ya existe");}
                
                //subo el archivo
                 move_uploaded_file($this->file['tmp_name'], $rutaFinal);
                
                 return $rutaReturn;
            }
            catch (UploadError $e) {$this->error = $e->getMessage();}
            catch (Exception $e) {$this->error = $e->getMessage();}

        }

        /*
        * Función que se encarga de añadir al archivo fotos.txt el titulo y la ruta de la
        * fotografía recien subida
        * Entradas:
        *       $file_uploaded: La ruta del archivo
        *       $title_uploaded: El titulo del archivo
        * Return: null
        */
        function addPictureToFile($file_uploaded,$title_uploaded){
            try {
                //abre el archivo
                 $archivo = fopen($_SERVER['DOCUMENT_ROOT']."/practicas/galeria/list.txt", "a");
                
                //concatenar los textos
                $textoAEscribir=$title_uploaded."###".$file_uploaded."\n";
    
                //escribir en el archivo ese texto
                //mirar que haya enter
                 fwrite($archivo,$textoAEscribir);
                
                //cierra archivo
                 fclose($archivo);
            
            } catch (Exception $e) {$this->error = $e->getMessage();}
        }

        /*
        * Getters: No tocar
        */
        function getError(){
            return $this->error;
        }
        
        function getFile(){
            return $this->file;
        }
        

    }
    

?>