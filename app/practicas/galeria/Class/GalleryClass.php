<?php
include('./Class/PictureClass.php');//incluye el picture


    class Gallery{
        private $_gallery = [];
        private $fileName;

      /*Constructor: Recibe la ruta del archivo fotos.txt*/
        function __construct($fileName){
          $this->fileName=$fileName;
          $this->loadGallery();
        }
    
        /*
      *Recorre el archivo fotos.txt y para cada linea, crea un
      *elemento Picture que lo añade al atributo $_gallery[]
      */
        function loadGallery(){
          //mirar si existe
          if(file_exists($this->fileName)){
            try{
              //abrir el archivo
              $archivo = fopen($this->fileName, "r");
              $lecturaTotal=fread($archivo, 10000000);

              $separadoEnter=explode("\n",$lecturaTotal);

              for ($contador=0; $contador<count($separadoEnter); $contador++){
                //mirar si no es un espacio en blanco o nulo o enter
                if($separadoEnter[$contador]!="" || $separadoEnter[$contador]!=" " || $separadoEnter[$contador]!="\n" || $separadoEnter[$contador]!=null){
                  //separar por ###
                  $separado=explode("###",$separadoEnter[$contador]);
    
                  //crear un elemento picture 
                  $picture=new Picture($separado[0],$separado[1]);
    
                  //añadirlo a la array
                  array_push($this->_gallery,$picture);
                }
              }
    
              //lo cierra
              fclose($archivo);
  
            } catch (Exception $e) {$this->error = $e->getMessage();}
  
          }
          

        }
    
        /*
      *Getters.
      */
        public function getGallery(){return $this->_gallery;}



        public function deleteImage($nombre,$posicion){
          // echo $nombre;die();
          $archivoName=$_SERVER['DOCUMENT_ROOT'].$nombre;

          //borra la imagen
          unlink($archivoName);

          //abre el txt para leer
          $archivo = fopen($this->fileName, "r");
          
          //leo todo el archivo
          $lecturaTotal=fread($archivo, filesize($this->fileName)+1);
          
          //cerrar
          fclose($archivo);
          
          $separadoEnter=explode("\n",$lecturaTotal);
                    
          //quitarlo de la array
          unset($separadoEnter[$posicion]);
          
          //borrar archivo
          unlink($this->fileName);

          //abrir el archivo
          $archivo = fopen($this->fileName, "w");
          
          
          fwrite($archivo,implode("\n",$separadoEnter));//escribe
          
          //lo cierra
          fclose($archivo);

          //quitarlo de la array de galery
          
          
          // die();
          header('Location: gallery.php');


        }
    }
?>