<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
<?php include_once('_header.php') ?>
<div class="card">
    <div class="card-body">
        <div class="bd-example">
            <a type="button" class="btn btn-primary" href="addPicture.php">Add picture</a>
            <a type="button" class="btn btn-success" href="gallery.php?gallery=true">View Gallery</a>
            <a type="button" class="btn btn-danger" href="gallery.php">Delete image</a>
        </div>
    </div>
</div>
<?php include_once('_footer.php') ?>




</body>
</html>