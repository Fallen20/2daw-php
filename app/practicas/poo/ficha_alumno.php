<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <?php include_once('_header.php') ?>

    <p style="font-weight:bold;">1. Crear una pagina que permite ingresar los datos de un alumno(ficha_alumno.php). Al enviar el formulario aparecerá en una nueva página (ficha_alumno_view.php) todos los datos introducidos anteriormente, incluida la fotografía:</p>
<hr>
<form method="post" action="ficha_alumno_view.php" enctype="multipart/form-data">
        <p>
            Nombre: <input type="text" name="nombre">
        </p>
        <p>
            Apellido: <input type="text" name="apellido">
        </p>

        
        <p>
            Foto: <input type="file" name="archivo">
        <!-- si es multiplo se envia como array,sino solo -->
        </p>
        
        <p>
            Direccion: <input type="text" name="direccion">
        </p>

        <p>
            Comentarios: <input type="text" name="comentarios">
        </p>

        <label>Selecciona las materias en las que te quieres matricular:</label> <br>
        <label><input type="checkbox" name="asignaturas[]" value="m07"> M07</label><br>
        <label><input type="checkbox" name="asignaturas[]" value="m08"> M08</label><br>
        <label><input type="checkbox" name="asignaturas[]" value="m09"> M09</label><br>
        <label><input type="checkbox" name="asignaturas[]" value="m06"> M06</label><br>
        <label><input type="checkbox" name="asignaturas[]" value="m12"> M012</label><br>
        <label><input type="checkbox" name="asignaturas[]" value="m13"> M013</label><br>

        <input type="submit" value="Enviar">
    </form>

    <?php include_once('_footer.php') ?>

</body>
</html>