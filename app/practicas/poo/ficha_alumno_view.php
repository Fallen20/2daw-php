<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
<?php include_once('_header.php') ?>

    <?php
    include('./Class/UploadClass.php');
    include('./Class/PersonaClass.php');

    //creamos clase y le pasamos el archivo
    $upload = new Upload($_FILES['archivo']);

    
    try{ 
        //lo subimos
        $upload->upload();
        
        
        $persona = new Persona();
        $persona->setPicture($upload->getPath());
    
        
        $persona->toString();
    }
    catch(UploadError $e) {
        echo $e->errorMessage();
    }
    
    
    ?>
    <?php include_once('_footer.php') ?>

</body>
</html>