<?php
    //clase persona
    class Persona{
        private $nombre;
        private $apellido;
        private $direccion;
        private $comentarios;
        private $foto;
        private $asignaturas;
  

        //constructor
        function __construct(){
            $this->nombre=($_POST['nombre']);
            $this->apellido=($_POST['apellido']);
            $this->direccion=($_POST['direccion']);
            $this->comentarios=($_POST['comentarios']);
            $this->asignaturas=($_POST['asignaturas']);
            $this->foto=null;

        }

        

        public function setName($nombre){$this->nombre=$nombre;}
        public function setSurname($apellido){$this->apellido=$apellido;}
        public function setPicture($foto){$this->foto=$foto;}
        public function setAddress($direccion){$this->direccion=$direccion;}
        public function setComment($comentarios){$this->comentarios=$comentarios;}
        public function setSubjects($asignaturas){$this->asignaturas=$asignaturas;}

        public function getName(){return $this->nombre;}
        public function getSurname(){return $this->apellido;}
        public function getAddress(){return $this->direccion;}
        public function getPicture(){return $this->foto;}
        public function getComment(){return $this->comentarios;}
        public function getSubjects(){return $this->asignaturas;}


        public function toString(){
            
            echo '<strong>Nombre:</strong> '.$this->nombre .
            '<br><strong>Apellido:</strong> '.$this->apellido .
            '<br><strong>Direccion:</strong> '.$this->direccion .
            '<br><strong>Comentarios:</strong> '.$this->comentarios .'<br>';
            

            echo '<br><strong>Materias matriculadas:</strong> <ul>';
            for ($contador=0; $contador<count($this->asignaturas) ; $contador++) { 
                echo '<li>'.$this->asignaturas[$contador].'</li>';
            }
            echo '</ul>';


            if($this->foto!=null){
                echo '<p><strong>Foto:</strong></p><img src= "/practicas/poo/subidas/'.$this->foto.'">';
            }
            else{echo '<strong>Foto:</strong> No se ha adjuntado foto';}
            
           
        }
    }
    ?>