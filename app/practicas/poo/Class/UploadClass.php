<?php
        class Upload{

            private $archivo;
            
            function __construct($name){
                $this->archivo=$name;
            }
        
          public function upload(){

            $extensionImagen = strtolower(pathinfo($this->archivo["name"],PATHINFO_EXTENSION));


            //mirar que sea una foto
            if($extensionImagen != "jpg" && $extensionImagen != "png" && $extensionImagen != "jpeg"
            && $extensionImagen != "gif" ) {
                throw new UploadError();
                //echo "Solo se aceptan JPG, JPEG, PNG o GIF";
            }
            else{
                $rutaArchivo = $_SERVER['DOCUMENT_ROOT']."/practicas/poo/subidas/".$this->archivo['name'];//ruta donde se va a guardar
                // $rutaArchivo = "subidas/".$this->archivo["name"];//ruta donde se va a guardar. Si solo es el nombre se guarda en la carpeta 'main'

                move_uploaded_file($this->archivo["tmp_name"], $rutaArchivo );//lo sube al server
            }

            
        }
        //saca la imagen por pantall
            public function getPath(){
                return $this->archivo['name'];
            }
        
        
        }
          /*
          * OPCIONAL:
          * Clase personalizada extendida de Exception que utilizaremos para lanzar errores
          * en la subida de archivos. Por ejemplo:
          * throw new UploadError("Error: Please select a valid file format.");
          */
          class UploadError extends Exception{
            public function errorMessage() {
                return "Error: Solo se aceptan JPG, JPEG, PNG o GIF";
            }
        }

    ?>