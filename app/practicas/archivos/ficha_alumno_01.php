<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
<?php include_once('_header.php') ?>
    <p><strong>1. Crear una pagina que permite ingresar los datos de un alumno (ficha_alumno_01.php). Al enviar aparecerá en una nueva página (ficha_alumno_02.php) con todos los datos introducidos anteriormente:</strong></p>
    <ul>
        <li>Nombre</li>
        <li>Apellido</li>
        <li>Fotografia</li>
        <li>Dirección</li>
        <li>Comentarios</li>
        <li>Lista Asignaturas Matriculadas (Checkbox)</li>
    </ul>
<hr>
    <form method="post" action="ficha_alumno_02.php" enctype="multipart/form-data">
        Nombre: <input type="text" name="nombre">
        <p></p>
        Apellido: <input type="text" name="apellido">
        <p></p>

        Foto: <input type="file" name="archivo">
        <!-- si es multiplo se envia como array,sino solo -->
        <p></p>

        Direccion: <input type="text" name="direccion">
        <p></p>

        Comentarios: <input type="text" name="comentarios">
        <p></p>

        <label>Selecciona las materias en las que te quieres matricular:</label> <br>
        <label><input type="checkbox" name="m07"> M07</label><br>
        <label><input type="checkbox" name="m08"> M08</label><br>
        <label><input type="checkbox" name="m09"> M09</label><br>
        <label><input type="checkbox" name="m06"> M06</label><br>
        <label><input type="checkbox" name="m12"> M012</label><br>
        <label><input type="checkbox" name="m13"> M013</label><br>

        <input type="submit" value="Enviar">
    </form>
    <?php include_once('_footer.php') ?>

</body>
</html>