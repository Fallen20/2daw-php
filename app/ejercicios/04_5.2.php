<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <?php
    //recuperar los campos

    $nombreArchivo=$_POST['nombre'];
    $nombreCompleto=$nombreArchivo.".txt";
    $encontrado=false;

    $rutaArchivo = ($_SERVER['DOCUMENT_ROOT']."/subidas/" . $nombreCompleto);


    //buscar si existe
    foreach (scandir($_SERVER['DOCUMENT_ROOT']."/subidas/") as $file) {
        
        if(basename($file)==$nombreCompleto) $encontrado=true;
    }

    //lo ha encontrado
    if($encontrado){
        //crea el archivo con ese nombre y lo abre para escribir
        $archivo = fopen($rutaArchivo, "r");

        $leido="";

        while(!feof($archivo)){
            //feof es si lo has acabado de leer
            $leido=$leido.fgets($archivo);
            //por alguna razon += no lo

        }

        // //lo cierra
        fclose($archivo);

        echo "<p><strong>Contenido del fichero:</strong></p>";
        echo $leido;
    }
    else{
        echo "No se ha encontrado el archivo";
    }

    ?>
</body>
</html>