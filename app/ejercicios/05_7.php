<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <p>7. Realiza una función separar(lista) que tome una lista de números enteros y devuelva (no imprimir) dos listas ordenadas. La primera con los números pares y la segunda con los números impares.
    </p>

    <form action="#" method="post">
        <input type="submit" value="Ver resultado" name="submit">
    </form>

    <?php

        $listaNum = [-12, 84, 13, 20, -33, 101, 9];

        if(isset($_POST['submit'])){ //si se ha enviado, llama a la funcion
            echo 'Resultado que devuelve la función: <br>';
            $array=separar($listaNum);

            for ($contador=0; $contador<count($array); $contador++) { //recorrer la array que tiene las dos array
                if($contador==0){echo "Array par:<br>";}//la primera es la par
                else{echo "Array impar:<br>";}

                for ($contador2=0; $contador2<count($array[$contador]); $contador2++) {//recorrer las listas devueltas (par e impar)
                    if($contador2==0){echo "[".$array[$contador][$contador2].", "; }//primera posicion
                    else if($contador2==count($array[$contador])-1){echo $array[$contador][$contador2]."]";}//ultima posicion
                    else{echo $array[$contador][$contador2].", ";}//en medio
                    
                }
                echo "<hr>";
            }
        }
        function separar($listaOrig){
            //inicializar lista 
            $listaPar=array();
            $listaImpar=array();

             for ($contador=0; $contador <count($listaOrig) ; $contador++) { //recorrer la lista recibida
                if($listaOrig[$contador]%2==0){array_push($listaPar,$listaOrig[$contador]);}//si es par, se añade a la array de pares
                else{array_push($listaImpar,$listaOrig[$contador]);}//es impar, se añade a la lista de impares
             }
            

            //  print_r($listaImpar);
             return array($listaPar,$listaImpar);//se devuelve una array con ambas
           
        }

            
            
        
    ?>
</body>
</html>