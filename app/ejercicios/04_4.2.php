<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <?php
    //recuperar los campos

    $nombreArchivo=$_POST['nombre'];
    $linea1=$_POST['linea1'];
    $linea2=$_POST['linea2'];
    $nombreCompleto=$nombreArchivo.".txt";

    $rutaArchivo = ($_SERVER['DOCUMENT_ROOT']."/subidas/" . $nombreCompleto);

    //crea el archivo con ese nombre y lo abre para escribir
    $archivo = fopen($rutaArchivo, "w");


    //esto escribe en el archivo que digamos y la cosa que queramos
    fputs($archivo,$linea1."\n");
    fputs($archivo,$linea2);

    //lo cierra
    fclose($archivo);

    echo "Se ha escrito en el fichero";
    ?>
</body>
</html>