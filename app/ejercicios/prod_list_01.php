<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
</head>
<body>
    <p>
        2. Crear una aplicació composta per 3 paginas PHP amb formularis que permeti ingresar el nom y preu d'una llista de productes.
        <ul>
            <li>Primera página (prod_list_01.php): Formulari on s'ingresa el numero de productes</li>
            <li>Segona página(prod_list_02.php): Formulari on s'ingresa el nom y el preu de tots els productes</li>
            <li>Tercera página(prod_list_03.php): Taula on llista tots els productes. Si algun producte no s'ha introduit ni nom ni preu ha de apareixer la frase "Product not inserted"</li>
        </ul>

    </p>
    <form name="formulario" method="post" action="prod_list_02.php">
        Cuantos productos? <input type="text" name="productosNum" value="">
        <input type="submit" />
    </form>
</body>
</html>