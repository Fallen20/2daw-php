<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <p><strong>3. Una función que reciba como parámetros el valor del radio de la base y la altura de un cilindro y devuelva el volumen del cilindro, teniendo en cuenta que el volumen de un cilindro se calcula como Volumen = númeroPi * radio * radio * Altura siendo númeroPi = 3.1416 aproximadamente.</strong></p>

    <form action="#" method="post">
        <p>Altura: <input type="text" name="altura"></p>
        <p>Base: <input type="text" name="radioBase"></p>
        <input type="submit" value="Enviar" name="submit">
    </form>

    <?php

        //recuperar numeros
        if(isset($_POST['altura'])){$altura=$_POST['altura'];}
        if(isset($_POST['radioBase'])){$radioBase=$_POST['radioBase'];}
        //solo guarda si se ha introducido algo
        

        if(isset($_POST['submit'])){
            //si se ha enviado, llama a la funcion
            echo 'Resultado que devuelve la función: '.volumenCilindro($altura,$radioBase);
        }
        function volumenCilindro($altura,$radioBase){
             //mirar si no es nulo
             if($altura==null ||$radioBase==null){
                return 'Se ha recibido un input vacío';
            }
            else{
                    //mirar si son numero
                if(is_numeric($altura) && is_numeric($radioBase) ){// el && porque TODOS han de ser numeros, no solo uno
                    $resultado= pi()*($radioBase**2)*$altura;

                    $tmp=intval($resultado*100);
                    $resultado=$tmp/100;
                    //para devolver 2 decimales solo
                    
                    return $resultado;
                }
                else{return 'Alguno de los valores no es un número';}
            }
           
        }

            
            
        
    ?>
</body>
</html>