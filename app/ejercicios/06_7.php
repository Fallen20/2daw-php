<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <?php
        class CabeceraDePagina {
            private $titulo;
            private $posicion;
            private $colorFondo;
            private $colorTexto;

            function __construct($titulo,$posicion,$colorFondo,$colorTexto){
              $this->titulo=$titulo;

              //si no pasa nada por el parámetro, se pone este
              if($colorFondo==null){$this->colorFondo='red';}
              else{$this->colorFondo=$colorFondo;}
              
              if($colorTexto==null){$this->colorTexto='yellow';}
              else{$this->colorTexto=$colorTexto;}
              
              if($posicion==null){$this->posicion='right';}
              else{$this->posicion=$posicion;}

            }
            public function graficar(){
                echo '<h1 style="background-color: '.$this->colorFondo.'; text-align:'.$this->posicion.'; color: '.$this->colorTexto.';">'.$this->titulo.'</h1>';
            }
          }


        $cabecera=new CabeceraDePagina('Titulo',null,null,'brown');
        $cabecera->graficar();

    ?>
    <p>7.Codificar la clase CabeceraDePagina que nos muestre un título alineado con un determinado color de fuente y fondo. Definir en el constructor parámetros predeterminados para los colores de fuente, fondo y el alineado del título.</p>

    <p style=""></p>


    
</body>
</html>