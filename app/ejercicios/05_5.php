<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <p>5. Realiza una función llamada relacion(a, b) que a partir de dos números cumpla lo siguiente:
        <ul>
            <li>Si el primer número es mayor que el segundo, debe devolver 1.</li>
            <li>Si el primer número es menor que el segundo, debe devolver -1</li>
            <li>Si ambos números son iguales, debe devolver un 0.</li>
        </ul>
    </p>

    <form action="#" method="post">
        <p>Número 1: <input type="text" name="valor1"></p>
        <p>Número 2: <input type="text" name="valor2"></p>
        <input type="submit" value="Enviar" name="submit">
    </form>

    <?php

        //recuperar numeros
        if(isset($_POST['valor1'])){$valor1=$_POST['valor1'];}
        if(isset($_POST['valor2'])){$valor2=$_POST['valor2'];}
        //solo guarda si se ha introducido algo
        

        if(isset($_POST['submit'])){
            //si se ha enviado, llama a la funcion
            echo 'Resultado que devuelve la función: '.relacion($valor1,$valor2);
        }
        function relacion($valor1,$valor2){
             //mirar si no es nulo
             if($valor1==null ||$valor2==null){
                return 'Se ha recibido un input vacío';
            }
            else{
                    //mirar si son numero
                if(is_numeric($valor1) && is_numeric($valor2) ){// el && porque TODOS han de ser numeros, no solo uno
                    if($valor1<$valor2){return -1;}
                    else if($valor1>$valor2){return 1;}
                    else if($valor1==$valor2){return 0;}
                    
                }
                else{return 'Alguno de los valores no es un número';}
            }
           
        }

            
            
        
    ?>
</body>
</html>