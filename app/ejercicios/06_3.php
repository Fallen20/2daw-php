<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <?php
        class CabeceraPagina {
            private $titulo;
            private $posicion;
            private $colorFondo;
            private $tipografia;

            function inicializar($titulo,$posicion,$colorFondo,$tipografia){
              $this->titulo=$titulo;
              $this->posicion=$posicion;
              $this->colorFondo=$colorFondo;
              $this->tipografia=$tipografia;
            }
            public function mostrar(){
                echo '<h1 style="background-color: '.$this->colorFondo.'; text-align:'.$this->posicion.'; font-family: '.$this->tipografia.';">'.$this->titulo.'</h1>';
            }
          }


        $cabecera=new CabeceraPagina();
        $cabecera->inicializar('Ejercicio 3','left','orange','Arial');
        $cabecera->mostrar();

    ?>
    <p>3. Confeccionar una clase CabeceraPagina que permita mostrar un título, indicarle si queremos que aparezca centrado, a derecha o izquierda, además permitir definir el color de fondo y de la fuente. Todo esto son propiedades del objecto 'Titulo'</p>



    
</body>
</html>