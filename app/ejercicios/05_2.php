<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <p>2. Una función que reciba cinco números enteros como parámetros y devuelva el resultado de sumar los cinco números (tipo función, hay un valor devuelto). Asigna el resultado de una invocación a la función con los números 2, 5, 1, 8, 10 a una variable de nombre $tmp y muestra por pantalla el valor de la variable.</p>

    <form action="#" method="post">
        <p>Número 1: <input type="text" name="valor1"></p>
        <p>Número 2: <input type="text" name="valor2"></p>
        <p>Número 3: <input type="text" name="valor3"></p>
        <p>Número 4: <input type="text" name="valor4"></p>
        <p>Número 5: <input type="text" name="valor5"></p>
        <input type="submit" value="Enviar" name="submit">
    </form>

    <?php

        //recuperar numeros
        if(isset($_POST['valor1'])){$num1=$_POST['valor1'];}
        if(isset($_POST['valor2'])){$num2=$_POST['valor2'];}
        if(isset($_POST['valor3'])){$num3=$_POST['valor3'];}
        if(isset($_POST['valor4'])){$num4=$_POST['valor4'];}
        if(isset($_POST['valor5'])){$num5=$_POST['valor5'];}
        //solo guarda si se ha introducido algo
        

        if(isset($_POST['submit'])){
            //si se ha enviado, llama a la funcion
            echo 'Resultado que devuelve la función: '.funcionSuma($num1,$num2,$num3,$num4,$num5);
        }
        function funcionSuma($num1,$num2,$num3,$num4,$num5){
             //mirar si no es nulo
             if($num1==null ||$num2==null ||$num3==null ||$num4==null ||$num5==null){
                return 'Se ha recibido un input vacío';
            }
            else{
                    //mirar si son numero
                if(is_numeric($num1) && is_numeric($num2) && is_numeric($num3) && is_numeric($num4) && is_numeric($num5)){
                    // el && porque TODOS han de ser numeros, no solo uno
                    $tmp = ($num1+$num2+$num3+$num4+$num5);

                    return $tmp;
                }
                else{return 'Alguno de los valores no es un número';}
            }
           
        }

            
            
        
    ?>
</body>
</html>