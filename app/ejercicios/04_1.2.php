<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <?php
        if($_FILES["archivo"]["error"] > 0){
        ?>
        <p>"Ha habido un error:</p> <?php echo $_FILES["archivo"]["error"];?>
    <?php
        }
        else{
    ?>
        <p>
            <strong>Nombre del archivo:</strong> <?php echo $_FILES["archivo"]["name"];?>
        </p>
        <p>
            <strong>Extensión:</strong> <?php echo $_FILES["archivo"]["type"];?>
        </p>
        <p>
            <strong>Tamaño:</strong> <?php echo $_FILES["archivo"]["size"];?>
        </p>
        <p>
            <strong>Ruta donde se ha guardado:</strong> <?php echo $_FILES["archivo"]["tmp_name"];?>
        </p>
    <?php
        }
    ?>
</body>
</html>