<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <?php
        class Empleado {
            private $nombre;
            private $sueldo;

            function inicializar($nombre,$sueldo){
            //this->var es como el this.var 
                $this->nombre=$nombre;
                $this->sueldo=$sueldo;
            } 
            
            function has_to_pay_taxes(){
                echo "Nombre: ".$this->nombre . "<br>";
                echo "Sueldo: ".$this->sueldo. "<br>";

            	if($this->sueldo>3000){echo 'Ha de pagar impuestos';}
            	else{echo 'No ha de pagar impuestos';}
            }
        }

    ?>
    <p>1. Confeccionar una clase Empleado, definir como atributos su nombre y sueldo. Definir un método inicializar que lleguen como dato el nombre y sueldo. Plantear un segundo método que imprima el nombre y un mensaje si debe o no pagar impuestos (si el sueldo supera a 3000 paga impuestos)</p>

    <form method="post">
        <p>Nombre: <input type="text" name="nombre"></p>
        <p>Sueldo: <input type="text" name="sueldo"></p>
        <input type="submit" value="Enviar" name="submit">
    </form>

    <?php
        //recuperar
        if(isset($_POST['nombre'])){$nombre=$_POST['nombre'];}
        if(isset($_POST['sueldo'])){$sueldo=$_POST['sueldo'];}

		if(isset($_POST['submit'])){
            //crea un objeto clase empleado
            $empleado=new Empleado();

            //inicia
            $empleado->inicializar($nombre, $sueldo);
            //llamar al metodo de pago impuesto
            echo $empleado->has_to_pay_taxes();
        }
        
        

    ?>

    
</body>
</html>