<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <p>5. Confeccionar una clase Tabla que permita indicarle en el constructor la cantidad de filas y columnas. Definir otro metodo que podamos cargar un dato en una determinada fila y columna además de definir su color de fuente y fondo. Finalmente debe mostrar los datos en una tabla HTML</p>

    <?php
        class Tabla {
        private $mat=array();
        private $cantFilas;
        private $cantColumnas;
        private $colorLetra;
        private $fondo;

        public function __construct($fi,$co){
            $this->cantFilas=$fi;
            $this->cantColumnas=$co;
        }
        
        public function cargar($fila,$columna,$valor){
            $this->mat[$fila][$columna]=$valor;
        }
        public function cargarEstilo($fila,$columna,$valor,$fondo,$colorLetra){
            $this->mat[$fila][$columna]='<td style="background-color:'.$fondo.'; color:'.$colorLetra.';">'.$valor.'</td>';
        }

        private function inicioTabla(){echo '<table border="1">';}
        private function inicioFila(){echo '<tr>';}
        private function finFila(){echo '</tr>';}
        private function finTabla(){echo '</table>';}



        private function mostrar($fi,$co){
            echo '<td>'.$this->mat[$fi][$co].'</td>';
            
        }
        private function mostrarEstilo($fi,$co){
            echo $this->mat[$fi][$co];
            
        }
        
        // public function personalizar($fila,$columna, $colorLetra,$fondo){
        //     echo '<td style="background-color:'.$this->fondo.'; color:'.$this->colorLetra.';">'.$this->mat[$fila][$columna].'</td>';
        // }

        
        
        public function graficar(){
            //aqui sale todo asi que tenemos que llamar a metodos
            $this->inicioTabla();//<table>
            
            for ($contador=1; $contador<=$this->cantFilas; $contador++) { 
                $this->inicioFila();//td

                for ($contador2=1; $contador2<=$this->cantColumnas; $contador2++) { 

                    $this->mostrar($contador, $contador2);//a este se le pasa info que esta en la array
                }

                $this->finFila();//tr
            }
            $this->finTabla();//</table>
        }


        public function graficarEstilo(){
            //aqui sale todo asi que tenemos que llamar a metodos
            $this->inicioTabla();//<table>
            
            for ($contador=1; $contador<=$this->cantFilas; $contador++) { 
                $this->inicioFila();//td

                for ($contador2=1; $contador2<=$this->cantColumnas; $contador2++) { 

                    $this->mostrarEstilo($contador, $contador2);//a este se le pasa info que esta en la array
                }

                $this->finFila();//tr
            }
            $this->finTabla();//</table>
        }
        }

        $tabla1=new Tabla(2,3);
        $tabla1->cargar(1,1,"1");
        $tabla1->cargar(1,2,"2");
        $tabla1->cargar(1,3,"3");
        $tabla1->cargar(2,1,"4");
        $tabla1->cargar(2,2,"5");
        $tabla1->cargar(2,3,"6");
        
        $tabla2=new Tabla(2,3);
        $tabla2->cargarEstilo(1,1,"1", 'blue','brown');
        $tabla2->cargarEstilo(1,2,"2", 'red','blue');
        $tabla2->cargarEstilo(1,3,"3", 'orange','white');
        $tabla2->cargarEstilo(2,1,"4", 'green','yellow');
        $tabla2->cargarEstilo(2,2,"5", 'blue','red');
        $tabla2->cargarEstilo(2,3,"6", 'pink','green');

        
    ?>

    <p>Tabla 1:</p>
    <?php
        $tabla1->graficar();
    ?>
    <p>Tabla 2:</p>

    <?php
        $tabla2->graficarEstilo();
    ?>
</body>
</html>