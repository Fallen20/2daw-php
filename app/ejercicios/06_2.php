<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <?php
        class Vinculos {
            private $arrayLinks=array();

            function cargarOpcion($link){
                array_push($this->arrayLinks, $link);

            }
            function mostrar(){
                //recorrer array
                for ($contador=0; $contador<count($this->arrayLinks); $contador++) { 
                    echo '<li style="display:inline-block; margin-right:10px;">'.
                            '<a href="'.$this->arrayLinks[$contador].'">'.$this->arrayLinks[$contador].'</a>'.
                        '</li>';
                }
            }
        }

    ?>
    <p>2. Implementar una clase que muestre una lista de hipervínculos en forma horizontal (básicamente un menú de opciones)</p>

<!-- sin form -->

        <?php
        $vinculos= new Vinculos();

        $vinculos->cargarOpcion('http://www.google.com');
        $vinculos->cargarOpcion('https://wiki.inf-edt.org/ca/daw/m7/uf1/php-POO/php-ejercicios-poo-1');
        $vinculos->cargarOpcion('https://www.youtube.com/');

        ?>
        <nav>
            <ul>
            <?php $vinculos->mostrar();?>
            </ul>
        </nav>
        

    <!-- esto con form -->
    <!-- <form method="post">
        <p>Dime un link: <input type="text" name="link"></p>
        <input type="submit" value="Enviar" name="submit">
    </form>

    <?php

//NO HACE FALTA FORMULARIO PERO PREGUNTAR



    //SOBRE ESTO:
    //cada vez que se envia el form, se hace esto de 0. Si pones $vinculos=new Vinculos(); se crea SIEMPRE una array de 0, ya que es como si no se hubiese creadoo
    //para evitarlo, hacemos que la creación del objeto se ponga en la sesión que es algo como que hasta que no se cierra el navegador no se borra o algo así y evitamos que caada vez que apretamos a submit no se resetee y haga push en el de cargar

     session_start();
        if(!isset($_SESSION['vinculo'])){
            $_SESSION['vinculo'] = new Vinculos(); //
        }

        //  $vinculos=new Vinculos();
        //recuperar
        if(isset($_POST['link'])){$link=$_POST['link'];}

		if(isset($_POST['submit'])){
            //crea un objeto
           
            
            //añadir el link
            $_SESSION['vinculo']->cargarOpcion($link);

            //cargar mostrar
            
            ?>
            <ul>
                <?php
                    echo  $_SESSION['vinculo']->mostrar();
                ?>
                
            </ul>
        
        <?php
            
        }
    ?> -->

    
</body>
</html>