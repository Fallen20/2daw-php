<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <p>6. Confeccionar una clase Menu. Permitir añadir la cantidad de opciones que necesitemos. Mostrar el menú en forma horizontal o vertical, pasar a este método como parámetro el texto “horizontal” o “vertical”. El método mostrar debe llamar alguno de los dos métodos privados mostrarHorizontal() o mostrarVertical().</p>

    <?php
        class Menu {
            private $enlaces=array();
            private $titulos=array();

            public function cargarOpcion($en,$tit){//meter llas cosas enviadas en las arrays correspondientes
                $this->enlaces[]=$en;
                $this->titulos[]=$tit;
            }
            private function mostrarHorizontal()
            {
                echo '<nav><ul>';

                for($f=0;$f<count($this->enlaces);$f++){//recorrer array sacando los datos con tags html
                    echo '<li style="display:inline-block; margin-right:10px;"><a href="'.$this->enlaces[$f].'">'.$this->titulos[$f].'</a></li>';
                }
                
                echo '</ul></nav>';
            }
            private function mostrarVertical()
            {
                for($f=0;$f<count($this->enlaces);$f++) {//recorrer array
                echo '<a href="'.$this->enlaces[$f].'">'.$this->titulos[$f].'</a>';//sacar datos con html con los datos
                echo "<br>";
                }
            }

            public function mostrar($orientacion){
                //te pasan que tipo de menu

                if($orientacion=='vertical'){$this->mostrarVertical();}//si te ha pasado la palabra vertical, sacar el menu vertical
                else{$this->mostrarHorizontal();}//te ha pasado otra cosa (horizontal), saca el menu horizontal
            }
        }

        $menu1=new Menu();//crear menu
        //añadir cosas
        $menu1->cargarOpcion('http://www.lanacion.com.ar','La Nación');
        $menu1->cargarOpcion('http://www.clarin.com.ar','El Clarín');
        $menu1->cargarOpcion('http://www.lavoz.com.ar','La Voz del Interior');

        echo '<p>Menu horizontal:</p>';
        $menu1->mostrar("horizontal");//sacarlo
        echo '<br>';
        
        $menu2=new Menu();//crear menu
        //añadir cosas
        $menu2->cargarOpcion('http://www.google.com','Google');
        $menu2->cargarOpcion('http://www.yahoo.com','Yhahoo');
        $menu2->cargarOpcion('http://www.msn.com','MSN');
        
        echo '<p>Menu vertical:</p>';
        $menu2->mostrar("vertical");//sacarlo
    ?>
</body>
</html>