<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <p>
        4. Una función a la que se le pasa el precio de una compra y calcula el precio de venta mediante la siguientes reglas:
        <ul>
            <li>
                Si la compra no alcanza los $100, no se realiza ningún descuento.
            </li>
            <li>
                Si la compra está entre $100 y $499,99, se descuenta un 10%.
            </li>
            <li>
                Si la compra supera los $500, se descuenta un 15%.
            </li>
        </ul>
    </p>

    <form action="#" method="post">
        <p>Introduce el precio: <input type="text" name="precio"></p>
        <input type="submit" value="Enviar" name="submit">
    </form>

    <?php

        //recuperar numeros
        if(isset($_POST['precio'])){$precio=$_POST['precio'];}
        //solo guarda si se ha introducido algo


        if(isset($_POST['submit'])){
            //si se ha enviado, llama a la funcion
            echo 'Precio final de la cuenta: '.decuentoCompra($precio);
        }
        function decuentoCompra($precio){
            //mirar si no es nulo
            if($precio==null){return 'Se ha recibido un input vacío';}
            else{
                    //mirar si son numero
                if(is_numeric($precio)){// el && porque TODOS han de ser numeros, no solo uno
                    if($precio<100){return $precio;}
                    else if($precio>=100 && $precio<=499.99){
                        $descuento=$precio*10/100;
                    }
                    else if($precio>=500){
                        $descuento=$precio*15/100;
                    }
                    
                    return $precio-$descuento;
                }
                else{return 'Alguno de los valores no es un número';}
            }
        
        }

        
        

    ?>
</body>
</html>