<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>

<?php
//se envia una array asi que tenemos que recorrerla
    //print_r($_FILES["archivo"]["name"]);
    $array=$_FILES["archivo"]; //esto recupera todo
    //se cuentan los nombres/size.. porque el count no se puede hacer de la propia array

    for($contador = 0; $contador < count($array['name']); $contador++) {?>
        <?php
            if($array["error"][$contador] > 0){
        ?>
        <p>"Ha habido un error:</p> <?php echo $array["error"][$contador];?>
    <?php
        }
        else{
    ?>
        <p>Archivo <?php echo $contador;?>:</p>
        <p>
            <strong>Nombre del archivo:</strong> <?php echo $array["name"][$contador];?>
        </p>
        <p>
            <strong>Extensión:</strong> <?php echo $array["type"][$contador];?>
        </p>
        <p>
            <strong>Tamaño:</strong> <?php echo $array["size"][$contador];?>
        </p>
        <p>
            <strong>Ruta donde se ha guardado:</strong> <?php echo $array["tmp_name"][$contador];?>
        </p>
    <?php
        }
    ?> 
    <hr>
    <?php
    }

    ?>
    
</body>
</html>