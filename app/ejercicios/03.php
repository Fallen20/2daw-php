<html>
  <head>
    <title>PHP Test</title>
  </head>
  <body>
    <p>
        <strong>Haz una pagina web con un formulario y dos campos de texto y un desplegable que de a elegir entre los 5 operaciones aritmeticas básicas. El objetivo es que al introducir dos numeros y seleccionar la operación se cargue una nueva pagina con el resultado de dicha operación</strong>
    </p>

    <form name="formulario" method="get" action="03_2.php">
<!--       con action hace que al apretar a submit pasa a la pag dicha -->
      <p>Dime el primer número: <input type="text" name="valor1" value=""></p>
      
      <p>
        Dime el segundo número: <input type="text" name="valor2" value="">
      </p>
      
      <p>
        <label for="lang">Operación:</label>
        <select name="operaciones" id="operaciones">
            <option value="suma">Suma</option>  
            <option value="resta">Resta</option>  
            <option value="division">Division</option>  
            <option value="multiplicar">Multiplicar</option>  
            <option value="potencia">Potencia</option>  
        </select>
      </p>
      
      <input type="submit" value="Enviar a otra web" />
    </form>


    <form name="formulario" method="post" action="03.php">
<!--       con action hace que al apretar a submit pasa a la pag dicha -->
      <p>Dime el primer número: <input type="text" name="valor1_2" value=""></p>
      
      <p>
        Dime el segundo número: <input type="text" name="valor2_2" value="">
      </p>
      
      <p>
        <label for="lang">Operación:</label>
        <select name="operaciones" id="operaciones">
            <option value="suma">Suma</option>  
            <option value="resta">Resta</option>  
            <option value="division">Division</option>  
            <option value="multiplicar">Multiplicar</option>  
            <option value="potencia">Potencia</option>  
        </select>
      </p>
      
      <input type="submit" name="submit" value="Enviar a esta web" />
    </form>

    <?php 
// valores recuperados
       $valor1 = $_POST['valor1_2'];
       $valor2 = $_POST['valor2_2'];
       $operacion=$_POST['operaciones'];

        $signo='';
        $resultado=0;
        if(isset($_POST['submit'])){
    ?>
<p>
  Se han recuperado los siguientes valores del formulario:
</p>
    <ul>
      <li>
        Valor 1: 
        <?php echo $valor1; ?>
      </li>
       <li>
        Valor 2: 
        <?php echo $valor2; ?>
      </li>
    </ul>

    <p>Operación a hacer: 
      <?php echo $operacion; ?>
    </p>

    <?php
      
      

      
//mirar si son nulos
      if($valor1!=null && $valor2!=null){

        //mirar si son numeros
         if(is_numeric($valor1) && is_numeric($valor2)){

           switch($operacion){
             case 'suma':
               $signo='+';
               $resultado= ($valor1 + $valor2);
               break;

             case 'resta':
               $signo='-';
               $resultado=($valor1 - $valor2);
               break;

             case 'division':
               $signo='/';
               $resultado=$valor1 / $valor2;
               break;

             case 'multiplicar':
               $signo='*';
               $resultado=$valor1 * $valor2;
               break;

             default:
              $signo='**';
               $resultado=$valor1 ** $valor2;
               break;
             
           }
         }
        else{echo 'Alguno de los valores no es un numero';}
      }
      else{
        echo 'Alguno de los campos está vacío';
      }
    ?>

    <p>
      <?php
        echo $valor1 . $signo . $valor2 . ' = ' . $resultado;
      }
      ?>
    </p>
  </body>
</html>