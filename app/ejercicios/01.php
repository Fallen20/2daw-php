<html>
  <head>
    <title>PHP Test</title>
  </head>
  <body>
    <p>
      <strong>
        1. Crea una pagina en la que se almacenan dos numeros en variables y muestre en una tabla los valores que toman las variables y cada uno de los resultados de todas las operaciones aritmeticas.
      </strong>

    </p>
    <?php
    $variableA=3;
    $variableB=7;?>

    <table>
    <tr> 
      <td style="border:1px solid black;">
        <?php
          echo $variableA . ' * ' . $variableB . " = " . $variableA*$variableB;
        ?>
      </td>
    </tr>
      
    <tr> 
      <td style="border:1px solid black;">
        <?php
          echo $variableA . ' / ' . $variableB . " = " . $variableA/$variableB;
        ?>
      </td>
    </tr>

    <tr> 
      <td style="border:1px solid black;">
        <?php
          echo $variableA . ' + ' . $variableB . " = " . ($variableA+$variableB);
        ?>
      </td>
    </tr>

    <tr> 
      <td style="border:1px solid black;">
        <?php
          echo $variableA . ' - ' . $variableB . " = " . ($variableA-$variableB);
        ?>
      </td>
    </tr>
      
    </table>
    
  </body>
</html>