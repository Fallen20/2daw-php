<html>
  <head>
    <title>PHP Test</title>
  </head>
  <body>
    <?php 
// valores recuperados
       $valor1 = $_GET['valor1'];
       $valor2 = $_GET['valor2'];
       $operacion=$_GET['operaciones'];

        $signo='';
        $resultado=0;
    ?>
<p>
  Se han recuperado los siguientes valores del formulario:
</p>
    <ul>
      <li>
        Valor 1: 
        <?php echo $valor1; ?>
      </li>
       <li>
        Valor 2: 
        <?php echo $valor2; ?>
      </li>
    </ul>

    <p>Operación a hacer: 
      <?php echo $operacion; ?>
    </p>

    <?php
      
//mirar si son nulos
      if($valor1!=null && $valor2!=null){

        //mirar si son numeros
         if(is_numeric($valor1) && is_numeric($valor2)){

           switch($operacion){
             case 'suma':
               $signo='+';
               $resultado= ($valor1 + $valor2);
               break;

             case 'resta':
               $signo='-';
               $resultado=($valor1 - $valor2);
               break;

             case 'division':
               $signo='/';
               $resultado=$valor1 / $valor2;
               break;

             case 'multiplicar':
               $signo='*';
               $resultado=$valor1 * $valor2;
               break;

             default:
              $signo='**';
               $resultado=$valor1 ** $valor2;
               break;
             
           }
         }
        else{echo 'Alguno de los valores no es un numero';}
      }
      else{
        echo 'Alguno de los campos está vacío';
      }
    ?>

    <p>
      <?php
        echo $valor1 . $signo . $valor2 . ' = ' . $resultado;
      ?>
    </p>

  </body>
</html>